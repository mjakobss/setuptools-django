#!/usr/bin/env python
"""Build and install setuptools-django."""

from ez_setup import use_setuptools
use_setuptools('0.6')

from setuptools import setup, find_packages
setup(
    name='setuptools-django',
    version='0.1.0',
    packages=find_packages(),

    entry_points="""
        [distutils.commands]
        test = setuptools_django.commands.test:Test
        [distutils.setup_keywords]
        django_settings = setuptools_django.validators:check_django_settings
    """,

    test_suite='setuptools_django.tests',

    author='Mattias Jakobsson',
    author_email='mattis.jakobsson@gmail.com',
    description='Setuptools plugin to distribute django apps and projects',
    license='BSD',
    keywords='setuptools plugin django',
)
