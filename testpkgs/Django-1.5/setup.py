#!/usr/bin/env python
"""A Django 1.5 test application."""

import glob
import os.path


def find_setuptools_django_eggs():
    relpaths = glob.glob('../../dist/setuptools_django-*.egg')
    abspaths = map(os.path.abspath, relpaths)
    return list(map(lambda egg: 'file://' + egg, abspaths))


from setuptools import setup, find_packages
setup(
    name='testapp',
    version='0.1.0',
    packages=find_packages(),
    setup_requires=['setuptools-django'],
    dependency_links=find_setuptools_django_eggs(),

    tests_require=['Django>=1.5,<1.6'],
    django_settings={
        'INSTALLED_APPS': ('testapp',),
    },
)
