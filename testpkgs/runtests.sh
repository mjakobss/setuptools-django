#!/usr/bin/env bash
# Run simple tests of setuptools-django using different versions of Django.

error() {
    echo $* >&2
    exit 1
}

if [[ "${1}" == -h || "${1}" == --help ]]; then
    echo "Usage: $(basename $0) [-h | python-binary]"
    echo "Run simple tests using different versions of Django"
    exit 0
fi

python=$(which "${1:-python}" 2> /dev/null)
if [ $? -eq 0 ]; then
    echo "Using Python '${python}'"
else
    error "Unable to find Python binary '${python}'"
fi

virtualenv=$(which virtualenv 2> /dev/null)
if [ $? -eq 0 ]; then
    echo "Using virtualenv '${virtualenv}'"
else
    error "Unable to find virtualenv, is it installed?"
fi

basedir=$(readlink -f $(dirname "$0"))
cd "${basedir}"
"${virtualenv}" -p "${python}" env
. env/bin/activate
pip install ..
for pkgdir in Django-*; do
    ! python -c "import sys; sys.exit(sys.version_info.major == 3)" \
	&& [ ${pkgdir} == Django-1.4 ] && continue
    cd "${basedir}/${pkgdir}"
    echo
    echo "===================================================================="
    echo "Running test with" $(basename $PWD)
    python setup.py test || break
done
rm -rf "${basedir}/env"
