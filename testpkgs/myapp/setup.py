from setuptools import setup, find_packages
setup(
    name='myapp',
    version='0.1.0',
    packages=find_packages(),
    setup_requires=['setuptools-django'],
    install_requires=['Django>=1.4'],
    django_settings={
        'INSTALLED_APPS': ('myapp',),
    },
    dependency_links=[
        'git+https://bitbucket.org/mjakobss/setuptools-django#egg=setuptools_django',
    ],
)
