"""Unit and regression tests of setuptools-django's validators."""

from distutils.errors import DistutilsSetupError
import unittest

from setuptools_django import validators


class CheckDjangoSettingsTests(unittest.TestCase):

    """Unit and regression tests for the django settings validator."""

    check = lambda self, v: validators.check_django_settings(None, None, v)

    def test_check_with_dict(self):
        self.assertRaises(DistutilsSetupError, self.check, {})
        self.check({'INSTALLED_APPS': ()})

    def test_check_with_module_name(self):
        self.assertRaises(DistutilsSetupError, self.check, '')
        self.assertRaises(DistutilsSetupError, self.check, 'a-b')
        self.check('a')
        self.check('a.b')

    def test_check_with_wrong_type(self):
        self.assertRaises(DistutilsSetupError, self.check, [])
