"""Validators for setup keywords."""

from distutils.errors import DistutilsSetupError

from pkg_resources import MODULE


def check_django_settings(dist, attr, value):
    """Verify that *value* is a dictionary or a valid module name."""
    if isinstance(value, dict):
        if 'INSTALLED_APPS' not in value:
            msg = "%r must contain 'INSTALLED_APPS' if it is a settings dict"
            raise DistutilsSetupError(msg % attr)
    else:
        try:
            assert MODULE(value)
        except (TypeError, AssertionError):
            msg = "%r must be a settings dict or a valid module name (got %r)"
            raise DistutilsSetupError(msg % (attr, value))
