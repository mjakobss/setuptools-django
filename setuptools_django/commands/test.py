"""Distutils command to run tests using Django."""

import os

from setuptools.command.test import test as Base


class Test(Base):

    user_options = Base.user_options + [
        ('django-settings-module=', None,
         "The Python path to a Django settings module, e.g. "
         "'myproject.settings.main'. If this isn't provided, the "
         "DJANGO_SETTINGS_MODULE environment variable and the django_settings "
         "setup keyword argument will be checked, in that order."),
        ('failfast', None,
         "Tells Django to stop running the test suit after first failed "
         "test."),
        ('liveserver=', None,
         "Overrides the default address where the live server (used with "
         "LiveServerTestCase) is expected to run from. The default value is "
         "localhost:8081."),
    ]

    def initialize_options(self):
        Base.initialize_options(self)
        self.django_settings_module = None
        self.failfast = None
        self.liveserver = None

    def finalize_options(self):
        Base.finalize_options(self)
        self.django_settings_dict = None
        if self.django_settings_module is None:
            self.django_settings_module = \
                os.environ.get('DJANGO_SETTINGS_MODULE')
        if isinstance(self.distribution.django_settings, dict):
            self.django_settings_dict = self.distribution.django_settings
        elif self.django_settings_module is None:
            self.django_settings_module = self.distribution.django_settings

    def run(self):
        dist = self.distribution
        if dist.install_requires:
            dist.fetch_build_eggs(dist.install_requires)
        if dist.tests_require:
            dist.fetch_build_eggs(dist.tests_require)
        if self.test_suite or \
           self.django_settings_module or \
           self.django_settings_dict:
            cmd = ' '.join(filter(None, self.test_args))
            if self.dry_run:
                self.announce('skipping "unittest %s" (dry run)' % cmd)
            else:
                self.announce('running "unittest %s"' % cmd)
                self.with_project_on_sys_path(self.run_tests)

    def run_tests(self):
        if self.django_settings_module:
            msg = "using Django settings module '%s'"
            self.announce(msg % self.django_settings_module)
            os.environ['DJANGO_SETTINGS_MODULE'] = self.django_settings_module
        elif self.django_settings_dict:
            msg = "using supplied Django settings dict"
            self.announce(msg)
            self.configure_django()
        else:
            return Base.run_tests(self)

        try:
            from django import setup
        except ImportError:
            pass
        else:
            setup()

        from django.core.management import call_command
        test_labels = [] if self.test_suite is None else [self.test_suite]
        call_command(
            'test',
            *test_labels,
            verbosity=self.verbose,
            testrunner=self.test_runner,
            failfast=self.failfast,
            liveserver=self.liveserver
        )

    def configure_django(self):
        from django.conf import settings
        kwargs = self.django_settings_dict.copy()
        if 'DATABASES' not in kwargs:
            kwargs['DATABASES'] = {
                'default': {
                    'ENGINE': 'django.db.backends.sqlite3',
                },
            }
        settings.configure(**kwargs)
